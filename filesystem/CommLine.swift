//
//  CommLine.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation

// Extensión de String para extraer la primer palabra de un String
extension String {

    // Primer palabra de un String. El separador es el espacio
    var firstWord: String? {
        if self.isEmpty {
            return nil
        }
        var result = ""
        for c in self.characters {
            if c == " " {
                return result
            }
            result.append(c)
        }
        return result
    }

}

// Contiene todos los elementos de una linea de comandos
class CommLine {

    // Posibles comandos
    enum Command {
        case createUser(String)
        case createFile(String)
        case deleteUser(String)
        case deleteFile(String)
        case ls
        case lsMine
        case login(String)
        case logout
        case changeProprietary(String, String)
        case getPermissions(String)
        case setPermissions(String, String)
        case read(String, Int)
        case write(String, String)
        case delete(String, Int)
        case seek(String, Int)
        case print(String)
        case info(String)
        case open(String)
        case close(String)
        case exit
    }

    // Solicita información al usuario
    // Siempre retorna un String aunque sea el vacío
    // Para hacer mas util la solicitud se muestra un texto si se le asigna un String a la función
    // Por defecto no toma ningún valor
    static func requestInput(text: String? = nil) -> String {
        if let t = text {
            print(t)
        }
        return readLine() ?? ""
    }

    // Solicita una confirmación del usuario
    // Toma:
    // + text: Texto a mostrar (pregunta)
    // + yesOptions: Lista de Strings aceptados como respuesta afirmativa
    // + noOptions: Lista de Strings aceptados como respuesta negativa
    // + attemps: Cantidad de intentos (debe ser mayor a cero)
    static func requestConfirmation(text: String,
                                    yesOptions: [String] = ["s", "S", "y", "Y"],
                                    noOptions: [String] = ["n", "N"],
                                    attemps: Int = 3) -> Bool {
        assert(attemps > 0, "Para hacer una confirmación hay que preguntar al menos una vez")
        // Prepara el texto a mostrar en pantalla
        let yes = yesOptions.reduce("") {"\($0) \($1)"}
        let no = noOptions.reduce("") {"\($0) \($1)"}
        let t = "\(text) - \(yes) | \(no): "
        // pregunta
        for _ in 1...attemps {
            let response = requestInput(text: t)
            if let word = response.firstWord {
                // confirmado
                if yesOptions.contains(word) {
                    return true
                }
                // negado
                else if noOptions.contains(word) {
                    return false
                }
            }
            // Excedió cantidad de preguntas
            else {
                print("Error: \"\(response)\" no es un texto admisible")
            }
        }
        print("Ha fallado demasiadas veces, vuelva a intentarlo más tarde")
        return false
    }

    // Clase utulizada para convertir un String en un Comando válido por el sistema
    class Parser {

        // Divide un String en una lista de palabras
        // Las palabras se separan con los espacios
        // Para escrubir una secuencia de palabras y tmmarlas como una sola sentencia escribirlo entre comillas
        private static func preparse(_ s: String) throws -> [String] {
            var result = [String]()
            var literalString = false
            var tempWord = ""
            for char in s.characters {
                if char == "\"" {
                    if literalString {
                        result.append(tempWord)
                        tempWord = ""
                    }
                    literalString = !literalString

                }
                else if char == " " {
                    if literalString {
                        tempWord.append(char)
                    }
                    else if !tempWord.isEmpty {
                        result.append(tempWord)
                        tempWord = ""
                    }
                }
                else {
                    tempWord.append(char)
                }
            }
            if !tempWord.isEmpty {
                result.append(tempWord)
            }
            if literalString {
                throw FSError.inputError("String literal no cerrado")
            }
            return result
        }

        // Compruena si una lista de palabras es un comando
        // En caso afirmativo retorna la lista de argumentos solicitados
        // argumentos:
        // + words: Lista de palabras
        // + command: Lista de palabras que componen el comando
        // + argCount: Cantidad de argumentos que debería tomar el comando
        private static func isCommand(_ words: [String], command: [String], argCount: Int = 0) -> [String]? {
            if command.count + argCount > words.count {
                return nil
            }
            for i in 0..<command.count {
                if command[i] != words[i] {
                    return nil
                }
            }
            var result = [String]()
            for i in command.count..<command.count + argCount {
                result.append(words[i])
            }
            return result
        }

        // Toma un String e intenta generar un comando válido
        // En caso de no poder lanza una Excepción
        static func parse(_ s: String) throws -> Command {
            let input = try preparse(s)
            if let args = isCommand(input, command: ["create", "user"], argCount: 1) {
                return .createUser(args[0])
            }
            else if let args = isCommand(input, command: ["create", "file"], argCount: 1) {
                return .createFile(args[0])
            }
            else if let args = isCommand(input, command: ["delete", "user"], argCount: 1) {
                return .deleteUser(args[0])
            }
            else if let args = isCommand(input, command: ["delete", "file"], argCount: 1) {
                return .deleteFile(args[0])
            }
            else if let _ = isCommand(input, command: ["ls"]) {
                return .ls
            }
            else if let _ = isCommand(input, command: ["ls", "mine"]) {
                return .lsMine
            }
            else if let args = isCommand(input, command: ["login"], argCount: 1) {
                return .login(args[0])
            }
            else if let _ = isCommand(input, command: ["logout"]) {
                return .logout
            }
            else if let args = isCommand(input, command: ["change", "proprietary"], argCount: 2) {
                return .changeProprietary(args[0], args[1])
            }
            else if let args = isCommand(input, command: ["get", "permissions"], argCount: 1) {
                return .getPermissions(args[0])
            }
            else if let args = isCommand(input, command: ["set", "permissions"], argCount: 2) {
                return .setPermissions(args[0], args[1])
            }
            else if let args = isCommand(input, command: ["read", "file"], argCount: 2) {
                if let num = Int(args[1]) {
                    return .read(args[0], num)
                }
                throw FSError.inputError("El segundo agumento debe ser un número")
            }
            else if let args = isCommand(input, command: ["write", "file"], argCount: 2) {
                return .write(args[0], args[1])
            }
            else if let args = isCommand(input, command: ["delete", "file"], argCount: 2) {
                if let num = Int(args[1]) {
                    return .delete(args[0], num)
                }
                throw FSError.inputError("El segundo agumento debe ser un número")
            }
            else if let args = isCommand(input, command: ["seek", "file"], argCount: 2) {
                if let num = Int(args[1]) {
                    return .seek(args[0], num)
                }
                throw FSError.inputError("El segundo agumento debe ser un número")
            }
            else if let args = isCommand(input, command: ["print", "file"], argCount: 1) {
                return .print(args[0])
            }
            else if let args = isCommand(input, command: ["info", "file"], argCount: 1) {
                return .info(args[0])
            }
            else if let args = isCommand(input, command: ["open", "file"], argCount: 1) {
                return .open(args[0])
            }
            else if let args = isCommand(input, command: ["close", "file"], argCount: 1) {
                return .close(args[0])
            }
            else if let _ = isCommand(input, command: ["exit"]) {
                return .exit
            }
            throw FSError.inputError("\"\(s)\" no es un comando válido.")
        }

    }

}
