//
//  FileStructureManager.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation

class FileStructureManager<FS: FileStructure> where FS: FileOperationsProtocol {
    
    // Permisos:
    // r : lectura
    // w : escritura
    // d : destrucción
    
    // Representa los permisos existentes
    enum Permission {
        case read, write, destroy
    }
    
    // Máscaras de control
    fileprivate let readPermissionFlag: UInt8           = 0b00000001
    fileprivate let writePermissionFlag: UInt8          = 0b00000010
    fileprivate let destructivePermissionFlag: UInt8    = 0b00000100
    
    // Indicador de permisos globales
    fileprivate var globalPermissions: UInt8 = 0
    
    // Estructura de manejo de archivo
    private(set) var fileStructure: FS
    
    // Propietario del archivo
    var propietary: User
    
    // Usuario que está usando el archivo en este momento
    var openedBy: User? {
        didSet {
            fileStructure.resetIndex()
        }
    }
    
    // Nombre del archivo
    var filename: String {
        return fileStructure.filename
    }
    
    // Tamaño del archivo (En caracteres)
    var size: Int {
        return fileStructure.size
    }
    
    // Construye un archivo con un nombre y un usuario
    init(filename: String, user: User) {
        self.fileStructure = FS(filename: filename)
        self.propietary = user
    }
    
    // MARK: - Operaciones de estructura
    
    
    func checkCan(user: User, permission: Permission) throws {
        if propietary == user {
            return
        }
        switch permission {
        case .read:
            if (globalPermissions & readPermissionFlag) == 0 {
                throw FSError.fileError("Usted no tiene permiso para leer el archivo.")
            }
        case .write:
            if (globalPermissions & writePermissionFlag) == 0 {
                throw FSError.fileError("Usted no tiene permiso para escribir el archivo.")
            }
        case .destroy:
            if (globalPermissions & destructivePermissionFlag) == 0 {
                throw FSError.fileError("Usted no tiene permiso para destruir el archivo.")
            }
        }
    }
    
    private func canWrite(_ user: User) -> Bool {
        
        return (globalPermissions & writePermissionFlag) != 0
    }
    
    private func canRead(_ user: User) -> Bool {
        if propietary == user {
            return true
        }
        return (globalPermissions & readPermissionFlag) != 0
    }
    
    private func canDestroy(_ user: User) -> Bool {
        if propietary == user {
            return true
        }
        return (globalPermissions & destructivePermissionFlag) != 0
    }
    
    func canDoSomething(_ user: User) -> Bool{
        if propietary == user {
            return true
        }
        return globalPermissions != 0
    }
    
    func set(permissions: String) throws {
        var flags: UInt8 = 0
        for p in permissions.characters {
            switch p {
            case "r" : flags |= readPermissionFlag
            case "w" : flags |= writePermissionFlag
            case "d" : flags |= destructivePermissionFlag
            default: break //error
            }
        }
        self.globalPermissions = flags
    }
    
    func printPermissions(ofUser user: User) {
        print("Propietario: \"\(filename)\"")
        var perm = "Este usuario puede: "
        perm += canRead(user) ? "Leer" : ""
        perm += canWrite(user) ? "Escribir" : ""
        perm += canDestroy(user) ? "Destruir" : ""
        print(perm)
    }
    
}


