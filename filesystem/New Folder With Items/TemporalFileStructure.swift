//
//  TemporalFileStructure.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation


extension String {
    
    init(charlist: [Character]) {
        self.init()
        for char in charlist{
            self.append(char)
        }
    }
    
    var list: [Character] {
        var temp = [Character]()
        for char in self.characters {
            temp.append(char)
        }
        return temp
    }
    
}




class TemporalFileStructure: FileStructure, FileOperationsProtocol{
    
    private var content = [Character]()
    
    override var size: Int {
        return content.count
    }
    
    func readAll() -> String {
        return String(charlist: content)
    }
    
    func read(count: Int) -> String {
        if currentIndex > size {
            currentIndex = size
        }
        if currentIndex == size || count == 0 {
            return ""
        }
        currentIndex += 1
        return "\(content[currentIndex - 1])\(read(count: count - 1))"
    }
    
    func write(data: String) {
        content.insert(contentsOf: data.list, at: currentIndex)
    }
    
    func delete(count: Int) {
        if count == 0 && currentIndex == size {
            _ = content.remove(at: currentIndex)
            delete(count: count - 1)
        }
    }
    
    func seek(count: Int) {
        currentIndex += count
        if currentIndex < 0 {
            currentIndex = 0
        }
        else if currentIndex > size {
            currentIndex = size
        }
    }
    
}
