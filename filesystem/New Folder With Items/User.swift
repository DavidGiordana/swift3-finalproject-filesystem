//
//  User.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation

class User {
    
    var name: String
    
    var password: String
    
    
    init(name: String, password: String) {
        self.name = name
        self.password = password
    }    
    
}

extension User: Hashable {
    
    var hashValue: Int {
        return name.hashValue
    }
    
    static func ==(u1: User, u2: User)->Bool{
        return u1.name == u2.name
    }
    
}
