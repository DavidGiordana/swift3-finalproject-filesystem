//
//  FileStructure.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation

class FileStructure {
    
    var filename: String
    
    var size: Int {
        return 0
    }
    
    var currentIndex: Int = 0
    
    
    required init(filename: String){
        self.filename = filename
    }
    
    func resetIndex() {
        currentIndex = 0
    }
}

protocol FileOperationsProtocol {
    
    func readAll() -> String
    
    func read(count: Int) -> String
    
    func write(data: String)
    
    func delete(count: Int)
    
    func seek(count: Int)
    
}
