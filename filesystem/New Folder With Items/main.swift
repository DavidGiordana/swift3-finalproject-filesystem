//
//  main.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation

var fs = FileSystem<TemporalFileStructure>()
fs.run()


/*
var lastCommand: CommLine.Command?

repeat {
    print(">>>>")
    if let text = readLine() {
        do {
            lastCommand = try CommLine.Parser.parse(text)
            print(lastCommand!)
        } catch {print("error")}
    }
    
} while !(lastCommand?.isExit ?? false)
*/
/*
var index = 0
let file = TemporalFileStructure(filename: "asd")


func seek(count: Int) {
    let size = file.size
    index += count
    if index < 0 {
        index = 0
    }
    else if index > size {
        index = size
    }
}



file.write(data: "12345678901234567890")
print(file.readAll())
file.seek(count: 100)
print(file.read(count: 11))
print(file.read(count: 1))
print(file.read(count: 11))
file.write(data: "12345678901234567890")
file.seek(count: 1)
print(file.read(count: 1))





*/
