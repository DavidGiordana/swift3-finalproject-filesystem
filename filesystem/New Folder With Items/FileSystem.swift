//
//  FileSystem.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation

// Representa los distintos tipos de errores
enum FSError: Error {
    case inputError(String?) //Error de ingreso de datos
    case userError(String?)  //Error de usuario
    case fileError(String?)  //Error de archivo
}


class FileSystem<FS: FileStructure> where FS: FileOperationsProtocol {
    
    var files = [String: FileStructureManager<FS>]()
    
    var users = [String: User]()
    
    var currentUser: User?
    
    var running = false
    
    func run(){
        running = true
        while running {
            let line = CommLine.requestInput(text: "\(currentUser?.name ?? "") > ")
            do {
                let comm = try CommLine.Parser.parse(line)
                try exec(comm: comm)
            } catch let error {
                handle(error: error)
            }
            
        }
    }
    
    
    private func exec(comm: CommLine.Command) throws {
        switch comm {
        case let .createUser(name): try create(user: name)
        case let .createFile(name): try create(file: name)
        case let .deleteUser(name): try delete(user: name)
        case let .deleteFile(name): try delete(file: name)
        case .ls: try ls()
        case .lsMine: try ls(current: true)
        case let .login(name): try login(user: name)
        case .logout: try logout()
        case let .changePropietary(file, user): try changePropietary(file: file, toUser: user)
        case let .getPermissions(file): try getPermissions(file: file)
        case let .setPermissions(file, permissions): try setPermissions(file: file, permissions: permissions)
        case let .read(file, count): try read(file: file, count: count)
        case let .write(file, data): try write(file: file, data: data)
        case let .delete(file, count): try delete(file: file, count: count)
        case let .seek(file, count): try seek(file: file, count: count)
        case let .print(file): try printFile(file)
        case let .info(file): try info(file: file)
        case let .open(file): try open(file: file)
        case let .close(file): try close(file: file)
        case .exit: running = false
        }
    }
    
    private func handle(error: Error) {
        if let fserror = error as? FSError {
            switch fserror {
            case let .inputError(text): print("ERROR DE INGRESO: \(text ?? "")")
            case let .userError(text): print("ERROR DE USUARIO: \(text ?? "")")
            case let .fileError(text): print("ERROR DE ARCHIVO: \(text ?? "")")
            }
        }
    }
    
    
    // MARK: - Funciones del sistema de archivos
    
    private func create(user username: String) throws {
        // Si el usuario ya existe
        if users[username] != nil {
            throw FSError.userError("El usuario \"\(username)\" ya eiste")
        }
        
        //Solicita contraseña
        var password = ""
        while password.isEmpty {
            print("Ingrese una contraseña para el usuario \"\(username)\"")
            if let p = readLine(){
                password = p
            }
        }
        
        // Crea el usuario
        let user = User(name: username, password: password)
        users[username] = user
        print("Se ha creado el usuario correctamente")
    }
    
    private func create(file filename: String) throws {
        // Obtiene el usuario actual
        let currUser = try getCurrentUser()
        
        // Si el archivo existe
        if files[filename] != nil {
            throw FSError.fileError("El archivo \"\(filename)\" ya existe")
        }
        
        // Creea el archivo
        let fsm = FileStructureManager<FS>(filename: filename, user: currUser)
        files[filename] = fsm
        print("Archivo \"\(filename)\" creado")
    }
    
    private func delete(user username: String) throws {
        // Obtiene el usuario actual
        let currUser = try getCurrentUser()
        
        // Si no se autoriza la eliminación
        if !CommLine.requestConfirmation(text: "¿Quiere eliminar el usuario \"\(username)\"?") {
            return
        }
        
        // Comprueba la contraseña
        try checkPassword(forUser: username)
        
        // Comprueba si no se designaron los permisos
        let filesAsoc = files.values.filter {$0.propietary == currUser}
        guard filesAsoc.isEmpty else {
            throw FSError.fileError("Existen archivos asociados a este usuario. Ceda la posesión a otro usuario.")
        }
        
        // Elimina el usuario
        currentUser = nil
        users[username] = nil
        print("El usuario \"\(username)\" ha sido eliminado correctamente")
    }
    
    private func delete(file filename: String) throws {
        // Obtiene el usuario actual
        let currUser = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Pregunta si se quiere realmente eliminar el archivo
        if !CommLine.requestConfirmation(text: "¿Quiere eliminar el archivo \"\(filename)\"?") {
            return
        }
        
        // Si el archivo puede ser escrito
        try fsm.checkCan(user: currUser, permission: .write)
        
        // Elimina un archivo
        files[filename] = nil
        print("Archivo \"\(filename)\" eliminado")
    }
    
    private func ls(current: Bool = false) throws{
        // Obtiene el usuario actual
        let currUser = try getCurrentUser()
        
        for fsm in files.values {
            if !current || fsm.propietary == currUser {
                print(fsm.filename)
            }
        }
    }
    
    private func login(user username: String) throws {
        // El usuario no existe
        guard let user = users[username] else {
            throw FSError.userError("El usuario \"\(username)\" no eiste")
        }
        
        // Compruba la contraseña
        try checkPassword(forUser: username)
        
        //Inicia sesión
        currentUser = user
        print("Bienvenido \"\(username)\"")
    }
    
    private func logout() throws {
        // Comprueba que si hay una sesión iniciada
        let currUser = try getCurrentUser()
        
        // Cierra sesión
        currentUser = nil
        for (_, fsm) in files {
            if fsm.openedBy == currUser {
                fsm.openedBy = nil
            }
        }
        print("Sesión de \"\(currUser.name)\" cerrada")
    }
    
    private func changePropietary(file filename: String, toUser username: String) throws {
        // Comprueba que el usuario sea propietario del archivo
        try checkCurrentUserPropietary(file: filename)
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // El usuario no existe
        guard let toUser = users[username] else {
            throw FSError.userError("El usuario \"\(username)\" no existe.")
        }
        
        // Cambia el propietario
        fsm.propietary = toUser
        print("Se ha cambiado el propietario del archivo \"\(filename)\" al usuario \"\(username)\"")
    }
    
    private func getPermissions(file filename: String) throws {
        // Comprueba que si hay una sesión iniciada
        let currUser = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Imprime los permisos
        fsm.printPermissions(ofUser: currUser)
    }
    
    private func setPermissions(file filename: String, permissions: String) throws {
        // Comprueba que si hay una sesión iniciada
        let currUser = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Si no es el propietario
        guard fsm.propietary == currUser else {
            throw FSError.userError("Usted no es el propietario del archivo, no puede cambiar los permisos")
        }
        
        // Configura los permisos
        try fsm.set(permissions: permissions)
        print("Se han cambiado los permisos del archivo \"\(filename)\" a \"\(permissions)\"")
    }
    
    private func read(file filename: String, count: Int) throws {
        // Comprueba que si hay una sesión iniciada
        let currUser = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Compruena si un archivo está abierto por otro usuario
        try checkCurrentUserOpened(file: filename)
        
        // Si el archivo puede ser leido
        try fsm.checkCan(user: currUser, permission: .read)
        
        // Obtiene los datos
        print(fsm.fileStructure.read(count: count))
    }
    
    private func write(file filename: String, data: String) throws {
        // Comprueba que si hay una sesión iniciada
        let currUser = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Comprueba si un archivo está abierto por otro usuario
        try checkCurrentUserOpened(file: filename)
        
        // Comprueba si el archivo puede ser escrito
        try fsm.checkCan(user: currUser, permission: .write)
        
        // Guarda los datos
        fsm.fileStructure.write(data: data)
        print("Escritura exitosa")
    }
    
    
    private func delete(file filename: String, count: Int) throws {
        // Comprueba que si hay una sesión iniciada
        let currUser = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Compruena si un archivo está abierto por otro usuario
        try checkCurrentUserOpened(file: filename)
        
        // Comprueba si el archivo puede ser escrito
        try fsm.checkCan(user: currUser, permission: .write)
        
        // Elimina los datos
        fsm.fileStructure.delete(count: count)
        print("Borrado exitoso")
    }
    
    private func seek(file filename: String, count: Int) throws {
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Compruena si un archivo está abierto por otro usuario
        try checkCurrentUserOpened(file: filename)
        
        // Aplica el desplazamiento
        print(fsm.fileStructure.seek(count: count))
        print("Desplazamiento exitoso")
    }
    
    private func printFile(_ filename: String) throws {
        // Comprueba que si hay una sesión iniciada
        let currUser = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Compruena si un archivo está abierto por otro usuario
        try checkCurrentUserOpened(file: filename)
        
        // Si el archivo puede ser leido
        try fsm.checkCan(user: currUser, permission: .read)
        
        // Imprime la información
        print(fsm.fileStructure.readAll())
    }
    
    private func info(file filename: String) throws {
        // Comprueba que si hay una sesión iniciada
        let currUser = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Imprime la información
        printFileInfo(ofFile: fsm, user: currUser)
    }
    
    
    private func open(file filename: String) throws {
        // Comprueba que si hay una sesión iniciada
        let currUser = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // No tiene permiso de hacer nada
        guard fsm.canDoSomething(currUser) else {
            throw FSError.fileError("Usted no tiene ningún permiso para trabajar en este archivo")
        }
        
        // Si el archivo ya está abierto
        try checkFileIsClosed(filename)
        
        // Abre el archivo
        fsm.openedBy = currUser
        print("Archivo \"\(filename)\" abierto")
    }
    
    private func close(file filename: String) throws {
        // Comprueba que si hay una sesión iniciada
        _ = try getCurrentUser()
        
        // Obtiene el archivo
        let fsm = try get(file: filename)
        
        // Si el archivo está abierto por otro usuario
        try checkCurrentUserOpened(file: filename)
        
        // Cierra el archivo
        fsm.openedBy = nil
        print("Archivo \"\(filename)\" cerrado")
    }

    // MARK: - Funciones internas
    
    private func checkUser(name username: String, pass password: String) -> Bool {
        guard let user = users[username] else {
            return false
        }
        return user.name == username && password == user.password
    }
    
    private func checkPassword(forUser username: String) throws {
        for _ in 1...3 {
            let pass = CommLine.requestInput(text: "Ingrese la contraseña del usuario \"\(username)\": ")
            if checkUser(name: username, pass: pass) {
                return
            }
        }
        throw FSError.userError("Se ha ingresado mal la contraseña demasiadas veces, Intentelo más tarde.")
    }
    
    private func getCurrentUser() throws -> User {
        if let curr = currentUser {
            return curr
        }
        throw FSError.userError("No hay ninguna sesión en uso. Por favor inicie sesión en alguna cuenta.")
    }
    
    private func get(file filename: String) throws -> FileStructureManager<FS> {
        if let file = files[filename] {
            return file
        }
        throw FSError.fileError("El archivo \"\(filename)\" no existe")
    }
    
    private func printFileInfo(ofFile file: FileStructureManager<FS>, user: User) {
        print("*> \"\(file.filename)\" | Tamaño \(file.size) caracteres ")
        file.printPermissions(ofUser: user)
    }
    
    private func checkCurrentUserOpened(file filename: String) throws {
        let currUser = try getCurrentUser()
        let fsm = try get(file: filename)
        // Si el archivo fue abierto por otro usuario
        guard fsm.openedBy == currUser else {
            throw FSError.fileError("El archivo está abierto por otro usuario.")
        }
    }
    
    private func checkCurrentUserPropietary(file filename: String) throws {
        let currUser = try getCurrentUser()
        let fsm = try get(file: filename)
        // Si el archivo fue abierto por otro usuario
        guard fsm.propietary == currUser else {
            throw FSError.fileError("El archivo es porpiedad de otro usuario.")
        }
    }
    
    private func checkFileIsClosed(_ filename: String) throws {
        let file = try get(file: filename)
        if file.openedBy != nil {
            throw FSError.fileError("El archivo \"\(file.filename)\" está abierto.")
        }
    }
    
}
