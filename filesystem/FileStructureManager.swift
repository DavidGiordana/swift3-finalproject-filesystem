//
//  FileStructureManager.swift
//  filesystem
//
//  Created by davidgiordana on 4/1/17.
//  Copyright © 2017 David Giordana. All rights reserved.
//

import Foundation

// Implementación de manejador de archivo para un archivo
class FileStructureManager<FS: FileStructure> where FS: FileOperationsProtocol {

    // Permisos:
    // r : lectura
    // w : escritura
    // d : destrucción

    // Representa los permisos existentes
    enum Permission {
        case read, write, destroy
    }

    // Máscaras de control
    fileprivate let readPermissionFlag: UInt8           = 0b00000001
    fileprivate let writePermissionFlag: UInt8          = 0b00000010
    fileprivate let destructivePermissionFlag: UInt8    = 0b00000100

    // Indicador de permisos globales
    fileprivate var globalPermissions: UInt8 = 0

    // Estructura de manejo de archivo
    private(set) var fileStructure: FS

    // Propietario del archivo
    var proprietary: User

    // Usuario que está usando el archivo en este momento
    var openedBy: User? {
        didSet {
            fileStructure.resetIndex()
        }
    }

    // Nombre del archivo
    var filename: String {
        return fileStructure.filename
    }

    // Tamaño del archivo (En caracteres)
    var size: Int {
        return fileStructure.size
    }

    // Construye un archivo con un nombre y un usuario
    init(filename: String, user: User) {
        self.fileStructure = FS(filename: filename)
        self.proprietary = user
    }

    // MARK: - Operaciones de estructura

    // Comprueba si un usuario puede hacer alguna acción
    func checkCan(user: User, permission: Permission) throws {
        switch permission {
        case .read:
            if !canRead(user: user) {
                throw FSError.fileError("Usted no tiene permiso para leer el archivo.")
            }
        case .write:
            if !canWrite(user: user) {
                throw FSError.fileError("Usted no tiene permiso para escribir el archivo.")
            }
        case .destroy:
            if !canDestroy(user: user) {
                throw FSError.fileError("Usted no tiene permiso para destruir el archivo.")
            }
        }
    }
    
    // Comprueba si un usuario puede leer el archivo
    private func canRead(user: User) -> Bool {
        if proprietary == user {
            return true
        }
        return (globalPermissions & readPermissionFlag) != 0
    }

    // Comprueba si un usuario puede escribir el archivo
    private func canWrite(user: User) -> Bool {
        if proprietary == user {
            return true
        }
        return (globalPermissions & writePermissionFlag) != 0
    }

    // Comprueba si un usuario puede destruir el archivo
    private func canDestroy(user: User) -> Bool {
        if proprietary == user {
            return true
        }
        return (globalPermissions & destructivePermissionFlag) != 0
    }

    // Comprueba si un usuario puede hacer algo con el archivo
    func canDoSomething(_ user: User) -> Bool{
        if proprietary == user {
            return true
        }
        return globalPermissions != 0
    }

    // Configura los permisos de un archivo
    func set(permissions: String) throws {
        var flags: UInt8 = 0
        for p in permissions.characters {
            switch p {
            case "r" : flags |= readPermissionFlag
            case "w" : flags |= writePermissionFlag
            case "d" : flags |= destructivePermissionFlag
            default: throw FSError.fileError("El permiso \(p) no existe.")
            }
        }
        self.globalPermissions = flags
    }

    // Imprime los permisos de un archivo con un usario
    func printPermissions(ofUser user: User) {
        print("Propietario: \"\(filename)\"")
        var perm = "Este usuario puede: "
        perm += canRead(user: user) ? "Leer" : ""
        perm += canWrite(user: user) ? "Escribir" : ""
        perm += canDestroy(user: user) ? "Destruir" : ""
        print(perm)
    }

}
